var clientesObtenidos;

function getClientes() {
  var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);

  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innetText = JSONClientes.value[i].ContactName;

    var columnaCity = document.createElement("td");
    columnaCity.innetText = JSONClientes.value[i].City;

    var columnaBandera = document.createElement("td");
    var imgbandera = document.createElement("img");
    imgbandera.classList.add("flag");

    if(JSONClientes.value[i].Country == "UK") {
      imgbandera.src = rutaBandera + "UnitedKindom.png";
    } else {
      imgbandera.src = rutaBandera + JSONClientes.value[i].Country;
    }
    imgbandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    columnaBandera.appendChild(imgbandera);
    columnaBandera.innetText = JSONClientes.value[i].Country;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
